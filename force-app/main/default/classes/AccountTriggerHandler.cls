public without sharing class AccountTriggerHandler extends TriggerHandler {
    public AccountTriggerHandler() { }

    public override void beforeInsert() {}
    
    public override void afterInsert() {}
    
    public override void beforeUpdate() {}
    
    public override void afterUpdate() {}

}
