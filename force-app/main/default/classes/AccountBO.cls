public without sharing class AccountBO {
    private static final AccountBO instancia = new AccountBO();
    public static AccountBO getInstance() {
        return instancia;
    }
    public class AccountBOException extends Exception {	}
    
    public AccountBO() {}
    
}